# Site que mostra o valor do Bitcoin

Este site vai demonstrar qual o valor de venda do Bitcoin, utilizando a api disponibilizada em: https://www.mercadobitcoin.net/api/BTC/ticker/

## Requisitos

- Python
- Flask

## Instalando o projeto

Faça o git clone do projeto e em seguida crie o seu ambiente virtual para isto, execute o seguinte comando:

```sh
python3 -m pipenv install
pipenv shell
```

Instale o Flask para rodar nosso servidor web:
```sh
pipenv install flask
```

Para executar o código digite o seguinte comando:
```sh
python app.py
```

