import requests
from flask import Flask, render_template
from gnewsclient import gnewsclient

client = gnewsclient.NewsClient(
    language='portuguese brasil',
    location='Brazil',
    topic='Finanças'
)

app = Flask(__name__)

@app.route("/")
def index():
    requisicao = requests.get(
        'https://www.mercadobitcoin.net/api/BTC/ticker/'
        )
    valor_bitcoin = requisicao.json()
    valor = valor_bitcoin['ticker']['last']
    valor = round(float(valor), 2)
    return render_template("base.html", valor=valor)

@app.route('/news')
def news():
    news = client.get_news()
    return render_template("pagina_besta.html", news=news)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)